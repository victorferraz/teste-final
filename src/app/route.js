function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/message/message.html',
      controller: 'MessageController',
      controllerAs: 'message'
    });

  $urlRouterProvider.otherwise('/');
}

export default routerConfig;
