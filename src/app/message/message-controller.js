class MessageController {

  constructor (API_URL, $timeout, MessageService, $scope) {
    'ngInject';
    this.messages = MessageService;
    this.dir = 1;
    this.messagesArray = [];
    this.totalPage = 0;
    this.atualPage = 0;
    this.setMessage();
    this.$scope = $scope;
  }

  getMessage () {
    return this.messages
      .getPage(this.atualPage)
      .then(data => {
        this.messagesArray = data.data;
      });
  }

  setMessage () {
    return this.messages
      .getTotal()
      .then(data => {
        this.totalPage = data.data.totalPages;
        return this.getMessage();
      });
  }


  paginate (dir) {
    if (dir === 1){
      this.atualPage++;
    } else {
      this.atualPage--;
    }
    this.getMessage();
  }
}

export default MessageController;
