class GithubContributorService {
  constructor ($log, $http, API_URL) {
    'ngInject';

    this.$log = $log;
    this.$http = $http;
    this.api = API_URL;
  }


  getTotal () {
    return this.$http.get(this.api + 'number-of-pages/')
      .then((response) => {
        return response;
      })
      .catch((error) => {
        this.$log.error('XHR Failed for Messages.\n' + angular.toJson(error.data, true));
      });
  }

  getPage (page) {
    return this.$http.get(this.api + page)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        this.$log.error('XHR Failed for Messages.\n' + angular.toJson(error.data, true));
      });
  }
}

export default GithubContributorService;
