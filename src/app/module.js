import config from './config';
import routerConfig from './route';
import runBlock from './run';
import MessageController from './message/message-controller';

import MessageService from './message/message-service';

angular.module('testeFinal', ['ui.router'])
  .constant('API_URL', 'http://private-f721ed-hotmart.apiary-mock.com/messages/')
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('MessageController', MessageController)
  .service('MessageService', MessageService);
