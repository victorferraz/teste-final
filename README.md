## Hotmart
- The source code is: /src
- The optimized code is: /dist

## Technologies used
- Angular with ecmascript 6
- Babel (es6 compiler)
- Gulp
- Yeoman Webapp

## Cloning
- git clone git@bitbucket.org:victorferraz/teste-final.git

## Install dependencies
- Install dependencies: `npm i -g  gulp bower`
- Install project and dependencies: `npm i` 
- Install client dependencies: `bower install` 

## Reference for organization:
- Angular Style Guide: (https://github.com/johnpapa/angular-styleguide)
- Angular Seed: (https://github.com/angular/angular-seed)

## Running the project
- You can use, just opening the html fiel in: src or dist. 
- You can run with webserver using `gulp serve`, after install all dependencies
